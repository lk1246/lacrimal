
from common_module import conn, read_data


cur = conn.cursor()

datas = read_data()

sql = "INSERT INTO patient (sex, birthday) VALUES (%(sex)s, %(birthday)s)"
sql2 = ("INSERT INTO TMH_DDT "
        "(pid, exam_date, TMH_rt, TMH_lt, DDT_rt, DDT_lt) "
        "VALUES "
        "(%(pid)s, %(pe_date)s, %(tear_h)s, %(tear_h2)s, %(ddt)s, %(ddt2)s)")

sql3= ("INSERT INTO syringe "
       "(pid, exam_date, obst_rt, obst_lt) "
        "VALUES "
        "(%(pid)s, %(pe_date)s, %(syr)s, %(syr2)s)")

sql4 = ("INSERT INTO dcg "
        "(pid, exam_date, obst_loc_rt, obst_loc_lt, obst_rt, obst_lt, obst_cause_rt, obst_cause_lt) "
        "VALUES "
        "(%(pid)s, %(dcg_date)s, %(dcg_level)s, %(dcg_level2)s, %(dcg_obst)s, %(dcg_obst2)s, %(dcg_cause)s, %(dcg_cause2)s)")

sql5 = ("INSERT INTO dacryoscopy "
       "(pid, exam_date, obst_loc_rt, obst_loc_lt, obst_rt, obst_lt, obst_cause_rt, obst_cause_lt) "
        "VALUES "
        "(%(pid)s, %(dacryo_date)s, %(dacry_level)s, %(dacry_level2)s, %(dacry_obst)s, %(dacry_obst2)s, %(dacry_cause)s, %(dacry_cause2)s)")


cur.execute(sql, datas[0])
datas[0]["pid"] = cur.lastrowid
print(cur.lastrowid)

sqls= [sql2, sql3, sql4, sql5]
for i in sqls:
    print(i)
    print(datas[0])
    cur.execute(i, datas[0])

conn.commit()



conn.close()

