DROP TABLE patient;
DROP TABLE TMH_DDT;
DROP TABLE syringe;
DROP TABLE dcg;
DROP TABLE dacryoscopy;

CREATE TABLE patient(
    pid SERIAL,
    sex CHAR(1),
    birthday DATE,
    PRIMARY KEY(pid)
    );

CREATE TABLE TMH_DDT(
    id SERIAL,
    pid BIGINT NOT NULL REFERENCES patient(pid),
    exam_date DATE,
    TMH_rt INT,
    TMH_lt INT,
    DDT_rt INT,
    DDT_lt INT
    );

CREATE TABLE syringe(
    id SERIAL,
    pid BIGINT NOT NULL REFERENCES patient(pid),
    exam_date DATE,
    obst_rt INT,
    obst_lt INT
    );


CREATE TABLE dcg(
    id SERIAL,
    pid BIGINT NOT NULL REFERENCES patient(pid),
    exam_date DATE,
    obst_loc_rt INT,
    obst_loc_lt INT,
    obst_rt INT,
    obst_lt INT,
    obst_cause_rt INT,
    obst_cause_lt INT
    );

CREATE TABLE dacryoscopy(
    id SERIAL,
    pid BIGINT NOT NULL REFERENCES patient(pid),
    exam_date DATE,
    obst_loc_rt INT,
    obst_loc_lt INT,
    obst_rt INT,
    obst_lt INT,
    obst_cause_rt INT,
    obst_cause_lt INT
    );

